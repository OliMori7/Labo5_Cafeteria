﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Labo5_Cafeteria.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Accueil</title>
    <link href="Styles/StyleSheet1.css" rel="stylesheet" />
</head>

<body>
    <form id="form1" runat="server">
        <header>
        <h1>Page d'accueil</h1>
            </header>
        <br />
    <div id="inscription">
        <h3>Inscription</h3>
        <asp:Label ID="Label1" runat="server" Text="Nom"></asp:Label>
        <br />
        <asp:TextBox ID="txtNom" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="Label2" runat="server" Text="Prénom"></asp:Label>
        <br />
        <asp:TextBox ID="txtPrenom" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Nom d'utilisateur"></asp:Label>
        <br />
        <asp:TextBox ID="txtUser" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="Label4" runat="server" Text="Mot de passe"></asp:Label>
        <br />
        <asp:TextBox ID="txtPwd" runat="server" TextMode="Password"></asp:TextBox>
        <br />
        
            <asp:Button ID="butInsert" runat="server" OnClick="butInsert_Click" Text="Inscription" />
        <br />
      
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
        <div id="login">
            <h3>Log in</h3>
            <asp:Label ID="Label5" runat="server" Text="Nom d'utilisateur"></asp:Label>
            <br />
            <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="Label6" runat="server" Text="Password"></asp:Label>
            <br />
            <asp:TextBox ID="txtPwdUser" runat="server" TextMode="Password"></asp:TextBox>
            <br />
            <br />

            <asp:Button ID="butLogin" runat="server" Text="Allez entrer une recette" OnClick="butLogin_Click" />
            <br />
            <asp:Label ID="lblMessageLogin" runat="server"></asp:Label>
        </div>
            <footer>
            <h4>Admin</h4>
            <asp:Label ID="Label7" runat="server" Text="User Name"></asp:Label>
            <asp:TextBox ID="txtAdminName" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="Label8" runat="server" Text="Password"></asp:Label>
            <asp:TextBox ID="txtAdminPwd" runat="server"></asp:TextBox>
                <br />
                <asp:Button ID="butAdmin" runat="server" Text="Accéder aux recettes" OnClick="butAdmin_Click" />
                <br />
                <asp:Label ID="lblMessageAdmin" runat="server" Text=""></asp:Label>
            </footer>
        </form>
</body>
</html>
