﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Labo5_Cafeteria
{
    public partial class Recettes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(ddlType.Items.Count < 3)
            { 
                ddlType.Items.Add("Entrée");
                ddlType.Items.Add("Plat principal");
                ddlType.Items.Add("Dessert");
            }
        }

        protected void butRecette_Click(object sender, EventArgs e)
        {
            using (Cafeteria db = new Cafeteria())
            {
                Recette temp = new Recette
                {
                    NomRecette = txtNomRecette.Text,
                    DateSoumission = calDate.SelectedDate,
                    Ingredient = txtIngredient.Text,
                    EtapePreparation = txtEtape.Text,
                    Type = ddlType.SelectedValue,
                    Photo = fuPhoto.FileName
                };

                if (temp != null)
                {
                    db.RecetteSet.Add(temp);
                    db.SaveChanges();
                    lblMessageRecette.Text = "Recette ajoutée";
                    txtNomRecette.Text = "";
                    txtIngredient.Text = "";
                    txtEtape.Text = "";
                    txtNomRecette.Focus();
                }
                else
                {
                    lblMessageRecette.Text = "Veuillez remplir tous les champs, svpS";
                }
            }
        }
    }
}