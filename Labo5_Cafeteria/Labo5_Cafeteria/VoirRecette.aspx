﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VoirRecette.aspx.cs" Inherits="Labo5_Cafeteria.VoirRecette" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Consulter les recettes</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h2>Vous cherchez une recette?</h2>
        <asp:Label ID="Label1" runat="server" Text="Par type"></asp:Label>
        <br />
        <asp:RadioButton ID="rbType" runat="server" GroupName="rbRecette" />
        <asp:DropDownList ID="ddlSearchType" runat="server"></asp:DropDownList>
        <br />
        <br />
        <asp:Label ID="Label2" runat="server" Text="Par date"></asp:Label>
        <br />
        <asp:RadioButton ID="rbDate" runat="server" />
        <asp:TextBox ID="txtDate" runat="server" TextMode="Date"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label3" runat="server" Text="Par date"></asp:Label>
        <br />
        <asp:RadioButton ID="rbPlageDate" runat="server" GroupName="rbRecette" />
        <asp:TextBox ID="txtPlageDate" runat="server" TextMode="Date"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="butSearch" runat="server" Text="Rechercher la recette" OnClick="butSearch_Click" />
        <asp:GridView ID="GridView1" runat="server"></asp:GridView>
    </div>
    </form>
</body>
</html>