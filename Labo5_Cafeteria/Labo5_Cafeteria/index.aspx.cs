﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Linq;
using System.Data.Entity.Validation;

namespace Labo5_Cafeteria
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void butInsert_Click(object sender, EventArgs e)
        {
            using(Cafeteria db = new Cafeteria())
            { 
                Client temp = new Client {

                Nom = txtNom.Text,
                Prenom = txtPrenom.Text,
                NomUtilisateur = txtUser.Text,
                Password = txtPwd.Text

                };
            
                db.ClientSet.Add(temp);
                db.SaveChanges();

                lblMessage.Text = "Inscription réussie";
                txtNom.Text = "";
                txtPrenom.Text = "";
                txtUser.Text = "";
                txtPwd.Text = "";
                txtNom.Focus();
            }
        }

        protected void butLogin_Click(object sender, EventArgs e)
        {
            using (Cafeteria db = new Cafeteria())
            {
                Client temp = (from login in db.ClientSet
                               where login.NomUtilisateur.Equals(txtUserName.Text) && login.Password.Equals(txtPwdUser.Text)
                               select login).FirstOrDefault();

                if(temp != null)
                {
                    Response.Redirect("Recettes.aspx");
                }
                else
                {
                    lblMessageLogin.Text = "Nom d'utilisateur ou mot de passe invalide";
                }
            }
        }

        protected void butAdmin_Click(object sender, EventArgs e)
        {
            using (Cafeteria db = new Cafeteria())
            {
                Proprietaire tempAdmin = (from loginAdmin in db.Proprietaire
                               where loginAdmin.NomAdmin.Equals(txtAdminName.Text) && loginAdmin.PasswordAdmin.Equals(txtAdminPwd.Text)
                               select loginAdmin).FirstOrDefault();

                if (tempAdmin != null)
                {
                    Response.Redirect("VoirRecette.aspx");
                }
                else
                {
                    lblMessageLogin.Text = "Nom d'admin ou mot de passe invalide";
                }
            }
        }
    }
}