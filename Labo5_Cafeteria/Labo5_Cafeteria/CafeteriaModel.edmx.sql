
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/29/2017 14:46:20
-- Generated from EDMX file: C:\Users\Olivier Morissette\Desktop\Labo5_cafeteria\Labo5_Cafeteria\Labo5_Cafeteria\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [dbCafeteria];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ClientSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ClientSet];
GO
IF OBJECT_ID(N'[dbo].[RecetteSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RecetteSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ClientSet'
CREATE TABLE [dbo].[ClientSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nom] nvarchar(max)  NOT NULL,
    [Prenom] nvarchar(max)  NOT NULL,
    [NomUtilisateur] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'RecetteSet'
CREATE TABLE [dbo].[RecetteSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [NomRecette] nvarchar(max)  NOT NULL,
    [DateSoumission] datetime,
    [Ingredient] nvarchar(max)  NOT NULL,
    [EtapePreparation] nvarchar(max)  NOT NULL,
    [Type] nvarchar(max)  NOT NULL,
    [Photo] nvarchar(max)
);
GO

-- Creating table 'ProprietaireSet
CREATE TABLE [dbo].[Proprietaire](
	[Id] int IDENTITY(1,1) NOT NULL,
	[NomProprio] nvarchar(max) NOT NULL,
	[Password] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'ClientSet'
ALTER TABLE [dbo].[ClientSet]
ADD CONSTRAINT [PK_ClientSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'RecetteSet'
ALTER TABLE [dbo].[RecetteSet]
ADD CONSTRAINT [PK_RecetteSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------