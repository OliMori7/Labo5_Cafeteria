﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Recettes.aspx.cs" Inherits="Labo5_Cafeteria.Recettes" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Ajout d'une recette</title>
    <link href="Styles/StyleSheet1.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <header><h1>Ajouter une recette</h1></header>
        <div id="blocAjout">
            <div id="ajout">
            <asp:Label ID="Label1" runat="server" Text="Entrez le nom : "></asp:Label>
         
            <asp:TextBox ID="txtNomRecette" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label2" runat="server" Text="Entrez la date : "></asp:Label>
            
            <!-- <asp:TextBox ID="txtDate" runat="server" TextMode="Date"></asp:TextBox> -->
            <asp:Calendar ID="calDate" runat="server" VisibleDate="0001-01-01" SelectedDate="0001-01-01" Height="100px" Width="100px"></asp:Calendar>
            <br />
            <asp:Label ID="Label3" runat="server" Text="Entrez les ingrédients : "></asp:Label>
            <br />
            <asp:TextBox ID="txtIngredient" runat="server" Width="200px" Height="100px" TextMode="MultiLine"></asp:TextBox>
            <br /><br />
            <asp:Label ID="Label4" runat="server" Text="Entrez les étapes de la préparation : "></asp:Label>
            <br />
            <asp:TextBox ID="txtEtape" runat="server" TextMode="MultiLine" Height="200px" Width="200px"></asp:TextBox>
            <br /><br />
            <asp:Label ID="Label5" runat="server" Text="Choisir le type de recette "></asp:Label>
            

            <asp:DropDownList ID="ddlType" runat="server"></asp:DropDownList>
            <br />
            <br />


            <asp:Label ID="Label6" runat="server" Text="Photo"></asp:Label>
            <br />
            <asp:FileUpload ID="fuPhoto" runat="server" />
            <br />
            <br />
            <asp:Button ID="butRecette" runat="server" Text="Ajouter la recette" OnClick="butRecette_Click" />
            <br />
            <asp:Label ID="lblMessageRecette" runat="server" Text=""></asp:Label>
            </div>
    </div>
    </form>
</body>
</html>
