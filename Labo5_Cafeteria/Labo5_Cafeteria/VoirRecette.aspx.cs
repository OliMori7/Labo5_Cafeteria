﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Labo5_Cafeteria
{
    public partial class VoirRecette : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ddlSearchType.Items.Count < 3)
            {
                ddlSearchType.Items.Add("Entrée");
                ddlSearchType.Items.Add("Plat principal");
                ddlSearchType.Items.Add("Dessert");
            }
        }

        protected void butSearch_Click(object sender, EventArgs e)
        {
            using (Cafeteria db = new Cafeteria())
            {
                if (rbType.Checked)
                { 
                    var result = from s in db.RecetteSet
                                 where s.Type.Equals(ddlSearchType.SelectedValue)
                                 select new { s.NomRecette, s.DateSoumission, s.Ingredient,
                                     s.EtapePreparation, s.Type, s.Photo };

                    GridView1.SelectMethod = "GetRecettes";
                }

                if (rbDate.Checked)
                {
                    var result = from search in db.RecetteSet
                                 where search.DateSoumission.Equals(txtDate.Text)
                                 select new { search.NomRecette, search.DateSoumission, search.Ingredient,
                                     search.EtapePreparation, search.Type, search.Photo };

                    GridView1.DataSource = result;
                    GridView1.DataBind();
                }

                if (rbPlageDate.Checked)
                {
                    var result = from search in db.RecetteSet
                                 where search.DateSoumission.Equals(txtPlageDate.Text)
                                 select new { search.NomRecette, search.DateSoumission, search.Ingredient,
                                     search.EtapePreparation, search.Type, search.Photo };

                    GridView1.DataSource = result;
                    GridView1.DataBind();
                }
            }
        }

        // Le type de retour peut être modifié en IEnumerable, toutefois pour prendre en charge
        //la pagination et le tri //, vous devez ajouter les paramètres suivants :
        //     int maximumRows
        //     int startRowIndex
        //     out int totalRowCount
        //     string sortByExpression

        public IEnumerable<Recette> GetRecettes()
        {
            List<Recette> lesRecettes;
            using (var db = new Cafeteria())
            {
                var result = from s in db.RecetteSet
                             where s.Type.Equals(ddlSearchType.SelectedValue)
                             select new
                             {
                                 s.NomRecette,
                                 s.DateSoumission,
                                 s.Ingredient,
                                 s.EtapePreparation,
                                 s.Type,
                                 s.Photo
                             };

                lesRecettes = db.RecetteSet.ToList();
            }

            return lesRecettes;
        }
    }
}